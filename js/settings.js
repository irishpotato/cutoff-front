// Выставляем сегодняшную дату и время
var today = new Date();
var year = today.getFullYear();
var month = today.getMonth() + 1; // январь это 0
var day = today.getDate();
var hour = today.getHours();
var min = today.getMinutes();

// Добавляем 0 в начало если месяц/число/час/минута меньше 10
month = (month < 10 ? "0" : "") + month;
day = (day < 10 ? "0" : "") + day;
hour = (hour < 10 ? "0" : "") + hour;
min = (min < 10 ? "0" : "") + min;

document.getElementById("date").value = year + '-' + month + '-' + day;
document.getElementById("time").value = hour + ':' + min;

updateModelParameters (document.getElementById("model").value);

// Добавляем список станций в селектор
var stationSelect = document.getElementById("station");
for (var i = 0; i < stations.length; i++) {
	var option = document.createElement('option');
	option.text = stations[i].name;
	stationSelect.add(option);
}

// Обновляем координаты в соответствии с первой выбранной станцией
updateStation();

// Вызывается каждый раз при изменении станции; меняет широту-долготу на соответсвующие ей.
function updateStation () {
	var station = stations[document.getElementById("station").selectedIndex];
	document.getElementById("latitude").value = station.latitude;
	document.getElementById("longitude").value = station.longitude;
}

// Вызывается каждый раз при изменении модели геомагнитного поля; показывает и скрывает нужные ей параметры.
function updateModelParameters (model) {
	switch(model) {
		case "dipole":
			setModelParameters([]);
			break;

		case "IGRF":
			setModelParameters([]);
			break;

		case "T89_Kp":
			setModelParameters(['kp_index']);
			break;

		case "T96_01":
			setModelParameters(['solar_wind_dynamic_pressure', 'dst_index', 'imf_by', 'imf_bz']);
			break;

		case "T01_01":
			setModelParameters(['solar_wind_dynamic_pressure', 'dst_index', 'imf_by', 'imf_bz', 'g1', 'g2']);
			break;
	}
}

function setModelParameters (params) {
	// Скрываем все параметры
	hideModelParameters(['solar_wind_dynamic_pressure', 'dst_index', 'imf_by', 'imf_bz', 'g1', 'g2', 'kp_index']);
	// Показываем только необходимые
	showModelParameters(params);
}

function showModelParameters (params) {
	for (var param in params) {
		var element = document.getElementById(params[param] + '_cell');
		element.style.display = 'table-cell';
		element.disabled = false;
	}
}

function hideModelParameters(params) {
	for (var param in params) {
		var element = document.getElementById(params[param] + '_cell');
		element.style.display = 'none';
		element.disabled = true;
	}
}

/*
function request (ip, settings) {
	var request = new XMLHttpRequest();
	request.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			console.log(this.responseText);
		}
	}

	request.open('GET', ip, false);
	request.setRequestHeader('cutoff_settings', settings);
	request.send();

	console.log(request);
	alert(request.status + "|" + request.responseText);
}*/

// При каждом новом расчёте вызывается requestId(), возвращающий id нового расчёта
function requestId (ip, settings) {
	var h = new XMLHttpRequest();
	h.open("GET", ip, false);
	h.setRequestHeader("cutoff_settings", settings);
	h.send();
	console.log(h);
	return h.responseText;
}

// Используя id расчёта requestCutoffDataById() возвращает
function requestCutoffDataById (id) {
	var h = new XMLHttpRequest();
	h.open("GET", "http://localhost", false);	// http://localhost?v
	h.setRequestHeader("user_id", id);
	h.send();
	console.log(h);
	return JSON.parse(h.responseText);
}

function submitSettings() {
	var model = document.getElementById('model').value;
	if (model == 'T89_Kp' || model == 'T96_01' || model == 'T01_01') {
		if (!confirm("Using T89_Kp and higher models usually takes a lot of time to calculate. Continue anyway?")) {
		    return;
		}
	}

	/*
	var button = document.getElementById('calculateButton');

	var percentage = 100.0;	// loading bottleneck
	button.value = 'Wait... ' + percentage + '%'; // loading bottleneck
	// cancel button ?*/

	var id = requestId('http://localhost', getSettingsString ());
	var data = requestCutoffDataById(id);
	console.log(data);
	showResult (data);
}

// Запаковывает форму с настройками в строку
function getSettingsString () {
	var modelId;
	switch (document.getElementById('model').value) {
		case 'dipole':
			modelId = '00';
			break;
		case 'IGRF':
			modelId = '10';
			break;
		case 'T89_Kp':
			modelId = '89';
			break;
		case 'T96_01':
			modelId = '96';
			break;
		case 'T01_01':
			modelId = '01';
			break;
	}

	//String.prototype.replaceAll = function(search, replace){
	//  return this.split(search).join(replace);
	//}

	var settings = '';
	//settings += document.getElementById('date').value.replaceAll('-', '.') + '|';

	settings += 'Input parameters for the program CUTOFF:|'
	settings += '01.07.2017' + '|'		//TODO: format data from yyyy-mm-dd to dd.mm.yyyy
	settings += document.getElementById('time').value + ':00|';	//TODO: get seconds
	settings += document.getElementById('solar_wind_dynamic_pressure').value + '|';
	settings += document.getElementById('dst_index').value + '|';
	settings += document.getElementById('imf_by').value + '|';
	settings += document.getElementById('imf_bz').value + '|';
	settings += document.getElementById('g1').value + '|';
	settings += document.getElementById('g2').value + '|';
	settings += document.getElementById('kp_index').value + '|';
	settings += modelId + '|';
	settings += document.getElementById('altitude').value + '|';
	settings += document.getElementById('latitude').value + '|';
	settings += document.getElementById('longitude').value + '|';
	settings += document.getElementById('vertical_angle').value + '|';
	settings += document.getElementById('azimutal_angle').value + '|';
	settings += document.getElementById('rigidity_lower_limit').value + '|';
	settings += document.getElementById('rigidity_upper_limit').value + '|';
	settings += document.getElementById('step').value + '|';
	settings += document.getElementById('max_time_of_flight').value + '|';
	settings += '0|';
	//settings = 'Input parameters for the program CUTOFF:|01.07.2017|00:00:00|0.5|-30.0|-7.8|-2.9|1.8|7.0|1.99|00|20.00|67.55|33.33|0.00|0.00|0.000|2.00|0.1|180.00|1|';

	return settings;
}

function showResult (data) {
	var result = document.getElementById('result');
	document.getElementById('exportPenumbraPngButton').disabled = false;
	document.getElementById('exportTraceGifButton').disabled = false;
	document.getElementById('3dWebglTracesButton').disabled = false;
	drawPenumbra(document.getElementById("penumbra"), data);
}

// Рисует пенумбру через <canvas>. В будущем надо бы рисовать и экспортировать через png, так будет лучше поддержка и проще использование.
function drawPenumbra (canvas, data) {
	var forbiddenColor = 'rgb(128, 128, 128)';
	var allowedColor = 'rgb(255, 255, 255)';

	var ctx = canvas.getContext("2d");

	ctx.beginPath();
	ctx.fillStyle = forbiddenColor;
	ctx.fillRect(0, 0, canvas.width / 2, canvas.height);
	ctx.fill();

	ctx.beginPath();
	ctx.fillStyle = allowedColor;
	ctx.fillRect(canvas.width / 2, 0, canvas.width / 2, canvas.height);
	ctx.fill();

	var penumbraOffset = 00;	// offset скорее всего не нужен будет потому как значения по краям будут выставливатся через cutoff.exe

	for (var i = 0; i < data.list.length; i++) {
		console.log(data.list[i].k);
		ctx.beginPath();
		// + 0.5 - фикс странной системы коорднат
		ctx.moveTo(penumbraOffset + i + 0.5, 0);
		//ctx.lineTo(penumbraOffset + i + 0.5, i >= canvas.width / 2 - penumbraOffset ? canvas.width / 2 - penumbraOffset - 40 : 40);
		ctx.lineTo(penumbraOffset + i + 0.5, 40);
	    ctx.strokeStyle = (data.list[i].k == 1 ? forbiddenColor : allowedColor);
	    ctx.stroke();
	}

	ctx.font = "24px Consolas";
	ctx.fillStyle = allowedColor;
	ctx.fillText("forbidden", 10, 90);
	ctx.fillStyle = forbiddenColor;
	ctx.fillText("allowed", 710, 90);
}

function showLocationOnMap () {
	var latitude = document.getElementById("latitude").value;
	var longitude = document.getElementById("longitude").value;
	var url = 'https://www.google.com/maps/place/' + latitude + ',' + longitude;
	window.open(url);
}

function exportPenumbraPng () {
	alert("Exporting penumbra png");
	var img = document.getElementById("penumbra").toDataURL("image/png");
	document.write('<img src="'+img+'"/>');
}

function exportTraceGif () {
	alert("Exporting trace gif");
}

function showWebglTraces () {
	alert("Showing Webgl traces");
}
